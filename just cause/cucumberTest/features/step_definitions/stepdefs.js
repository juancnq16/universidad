const assert = require('assert');
const { Given, When, Then } = require('cucumber');
var purchase = false;
class Customer{
    constructor(age, money){
        this.age = age;
        this.money = money;
    }
}
var customer = new Customer(18,2000);
Given('im above {int}', function(age){
    if (customer.age>=age){
        console.log("cool the age is ",age);
        purchase = true;
        return 'cool';
    }else{
        purchase = false;
        console.log("not cool, the age is ", age);
        return 'not cool';
    }
});
Then('I have more than {int}', function(cost){
    if (customer.money>=cost){
        console.log("enought for a ",cost," cigarrete");
        return 'cool';
    }else{
        console.log("not enought for a ",cost," cigarrete");
        return 'not cool';
    }
});
Then('I ask for a cigarrete {int}', function(cost){
    if (purchase){
        console.log("here is your cigarrete sir");
    }else{
        console.log("we dont sell cigarretes to kids")
    }
        return 'here is your cigarrete sir'; 
});
Then('I get a cigarrete {int}', function(cost){
    if(purchase){
        if (customer.money>=cost){
            console.log("thanks")
        }else{
            console.log("I will get more money")
        }
    }else{
        console.log("thanks for nothing")
    }
    return 'thanks';
});
Then('I lose {int}', function(cost){
    if(purchase && customer.money>=cost){            
        customer.money-=cost;
        console.log("now i have ",customer.money);
    }
    return 'succesfull transaction';
});
