Feature: buy
	I have money to buy cigarretes
    Scenario Outline: I wanna buy a cigarrete
        Given im above <age> 
        And I have more than <cigs>
        Then I ask for a cigarrete <cigs>
        Then I get a cigarrete <cigs>
        But I lose <cigs>
    Examples:
        | age | cigs |
        | 17 | 500 |
        | 18 | 600 |
        | 19 | 700 |
