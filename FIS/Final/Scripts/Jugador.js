class Jugador{
    constructor(casilla){
        this.casilla=casilla;
        this.puntaje=0;
        this.jugando=true;
        this.casilla.reproducir();
    }
    avanzar(casilla){
        if(this.casilla.escala != casilla.escala){
            if(this.casilla.tono != casilla.tono){
                this.casilla.detener();
                this.casilla = new casilla(casilla.escala, casilla.tono);
                this.casilla.reproducir();
            }else{
                this.casilla.detener();
                this.casilla = new Casilla(casilla.escala, this.casilla.tono)
                this.casilla.reproducir();
            }
        }else{
            if(this.casilla.tono != casilla.tono){
                this.casilla.detener();
                this.casilla = new Casilla(this.casilla.escala, casilla.tono);
                this.casilla.reproducir();
            }
        }
    }
}