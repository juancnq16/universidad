import random
from Nodo import *
vocales="aeiou"
consonantes="bcdfghjklmnpqrstvwxyz"
palabra = NodoN()
for letra in vocales: 
    temp=NodoN()
    temp.modValue(letra)
    palabra.addHijo(temp)
palabra.mezclar(list(consonantes))
print(len(palabra.hijos))
for i in range(0,len(palabra.hijos)):
    palabra.hijos[i].mezclar(vocales)
criterio=True
entrada=""
while criterio:
    entrada=entrada+input("digite la siguiente letra ")
    for hijo in palabra.hijos:
        if entrada==hijo.valor:
            palabra=hijo
            break
    if palabra.hijos:
        for sugerencia in palabra.hijos:
            res=input("quizas trataste de decir "+sugerencia.valor+palabra.hijos[random.randint(0,len(palabra.hijos)-1)].valor+"------->")
            if res=="si":
                criterio=False
                palabra=sugerencia.valor
                break
    else:
        criterio=False    
print("su palabra es: "+palabra)