class NodoN:
    def __init__(self):
        self.hijos=[]
        self.valor=None
    def addHijo(self,hijo):
        self.hijos.append(hijo)

    def modValue(self,value):
        self.valor=value
    def mezclar(self,coleccion):
        for i in range(0,len(self.hijos)):
            for elem in coleccion:
                nodo = NodoN()
                temp=self.hijos[i].valor+elem
                nodo.modValue(temp)
                self.hijos[i].addHijo(nodo)