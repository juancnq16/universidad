import random
def convertirLogico(arreglo):
    gusto=[]
    for elem in arreglo:
        if elem>50:
            gusto.append(True)
        else:
            gusto.append(False)
    return gusto
def sugerir(gusto):
    sugerencias=["subLvL1","quicaStein","laLlorona","flappyBird","simonDice","alienInvasion","snake"]
    if gusto[0] and gusto[4] and not gusto[6]:
        return sugerencias[6]
    if gusto[0] and gusto[4] and not gusto[6]:
        return sugerencias[4]
    if gusto[6] and gusto[4] and not gusto[0]:
        return sugerencias[0]
    if gusto[3] and gusto[2] and gusto[5] and not gusto[1]:
        return sugerencias[1]
    if gusto[3] and gusto[2] and not gusto[5] and gusto[1]:
        return sugerencias[4]
    if not gusto[3] and gusto[2] and gusto[5] and gusto[1]:
        return sugerencias[3]
    if gusto[3] and not gusto[2] and gusto[5] and gusto[1]:
        return sugerencias[2]
    return random.choice(sugerencias)
def pedirSugerencia(arreglo):
    gusto=convertirLogico(arreglo)
    return sugerir(gusto)