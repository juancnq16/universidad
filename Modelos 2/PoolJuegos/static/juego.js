var jugando;

$(document).ready(inicio)
$(document).keydown(controlador)

function inicio(){
    jugando=false;
    perdio=false;
    canvas = $("#canvas")[0];
	contexto = canvas.getContext("2d");
    buffer = document.createElement("canvas");
    contextoBuffer = buffer.getContext("2d");
    spawn = new Spawn();
    run();
}

function controlador(evento){
    //console.log("--- evento "+evento.which);
    if(evento.which==13){
        jugando=true;
        run();
    }else if(evento.which==8){
        jugando=false;
        //run();
    }else if(evento.which==80){
        spawn.reproducir();
    }else if(evento.which==82){
        spawn.detener();
    }else{
        if(spawn.juzgar(evento.which)){
            spawn.puntaje+=10;
            run();
            //spawn.dibujarPuntaje();
        }else{
            perdio=true;
            jugando=false;
            spawn.puntaje-=10;
            run();
        }
        //console.log("puntaje: "+spawn.puntaje);
    }
}
function run(){
    //jugando=false;
    buffer.width = canvas.width;
	buffer.height = canvas.height;
    contextoBuffer = buffer.getContext("2d");
    contextoBuffer.clearRect(0,0,buffer.width,buffer.height);
    if(document.getElementById("rip&tear").ended){
        spawn.dialogar("termino el juego!", contextoBuffer);
        enviarPuntaje(spawn.puntaje)
    }else{
        if(jugando){
            //aca va el codigo del juego
            spawn.ordenar(contextoBuffer);
        }else{
            if(perdio){
                spawn.detener(contextoBuffer);
            }else{
                spawn.dialogar("presiona enter para jugar", contextoBuffer);
            }
            //aca va el cuadro de dialogo
            
        }
    }
    
    contexto.clearRect(0,0,canvas.width,canvas.height);
	contexto.drawImage(buffer, 0, 0);
}
function enviarPuntaje(score){
	var puntaje = score;
	console.log(puntaje)
	var tupla = {'juego':'simonDice', 'puntaje':puntaje}
  	var evio = $.post("capturar", tupla)
		.fail(function(){
			alert("no se ha podido registrar su puntaje")
		})
		.done(function(){
			alert("puntaje registrado de forma exitosa")
		})
}