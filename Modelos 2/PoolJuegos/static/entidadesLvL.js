var Entities = {
    init: function (data) {
        var background = {
            sprite: new Entities.helpers.Sprite(data.spriteSheet, 0, 110, 256, 200),
            x: 0,
            y: 0,
            w: 768,
            h: 600
        };

        var jack = new Entities.helpers.Jack(data.spriteSheet, 60, 0, 63, 53);

        var exitPipe = new Entities.helpers.ExitPipe(624, 432, 144, 168);

		var score = new Entities.helpers.Score(530,20, "Monedas: ", true);
		
		var winText = new Entities.helpers.WinText(220,100, "** HAS GANADO **", false);
		
        var wallLocations = [[0, 0, 48, 600], [0, 528, 768, 72],
                             [192, 384, 336, 216], [726, 0, 42, 600]];

        var coinLocations = [[249, 150], [297, 150], [345, 150], [393, 150], [441, 150],
                             [201, 246], [249, 246], [297, 246], [345, 246], [393, 246], [441, 246], [489, 246],
                             [201, 342], [249, 342], [297, 342], [345, 342], [393, 342], [441, 342], [489, 342]]

        data.entities = {};

        data.entities.background = background;
		data.entities.score = score;
		data.entities.winText = winText;
        data.entities.jack = jack;
        data.entities.exitPipe = exitPipe;
        data.entities.wallsArray = [];
        data.entities.coinsArray = [];

        wallLocations.forEach(function (location) {
            data.entities.wallsArray.push(new Entities.helpers.Wall(location[0], location[1], location[2], location[3]));
        });

        coinLocations.forEach(function (location) {
            data.entities.coinsArray.push(new Entities.helpers.Coin(data.spriteSheet, location[0], location[1], 30, 42));
        });
    },

    helpers: {
        Sprite: function (img, srcX, srcY, srcW, srcH) {
            this.img = img;
            this.srcX = srcX;
            this.srcY = srcY;
            this.srcW = srcW;
            this.srcH = srcH;
        },

        Jack: function (img, x, y, w, h) {
            var self = this;
			this.juegoGanado = false;
            this.jumpSound = new Audio("audio/jump_01.wav");
			this.sprite = new Entities.helpers.Sprite(img, 0, 0, 32, 53);
			
            this.spriteAnimations = {
                walkRight: {
                    frames: [new Entities.helpers.Sprite(img, 229, 0, 30, 53),new Entities.helpers.Sprite(img, 260, 0, 26, 53),
                            new Entities.helpers.Sprite(img, 288, 0, 26, 53),new Entities.helpers.Sprite(img, 315, 0, 31, 53),
							new Entities.helpers.Sprite(img, 347, 0, 30, 53),new Entities.helpers.Sprite(img, 378, 0, 26, 53),
							new Entities.helpers.Sprite(img, 405, 0, 22, 53)
							],
                    currentFrame: 0
                },
                walkLeft: {
                    frames: [new Entities.helpers.Sprite(img, 266, 54, 30, 53),new Entities.helpers.Sprite(img, 239, 54, 26, 53),
                            new Entities.helpers.Sprite(img, 211, 54, 26, 53),new Entities.helpers.Sprite(img, 179, 54, 31, 53),
							new Entities.helpers.Sprite(img, 148, 54, 30, 53),new Entities.helpers.Sprite(img, 121, 54, 26, 53),
							new Entities.helpers.Sprite(img, 98, 54, 22, 53)
							],
                    currentFrame: 0
                },
				standRight: {
					frames: [new Entities.helpers.Sprite(img, 0, 0, 32, 53),new Entities.helpers.Sprite(img, 33, 0, 30, 53),
                            new Entities.helpers.Sprite(img, 64, 0, 32, 53),new Entities.helpers.Sprite(img, 97, 0, 32, 53),
							new Entities.helpers.Sprite(img, 130, 0, 32, 53),new Entities.helpers.Sprite(img, 163, 0, 32, 53),
							new Entities.helpers.Sprite(img, 196, 0, 32, 53)
							],
					currentFrame : 0
				},
                standLeft: {
					frames: [new Entities.helpers.Sprite(img, 493, 54, 32, 53),new Entities.helpers.Sprite(img, 462, 54, 30, 53),
                            new Entities.helpers.Sprite(img, 429, 54, 32, 53),new Entities.helpers.Sprite(img, 396, 54, 32, 53),
							new Entities.helpers.Sprite(img, 363, 54, 32, 53),new Entities.helpers.Sprite(img, 330, 54, 32, 53),
							new Entities.helpers.Sprite(img, 297, 54, 32, 53)
							],
					currentFrame : 0
				},
				
                jumpLeft: {
					frames: [new Entities.helpers.Sprite(img, 67, 54, 31, 53),new Entities.helpers.Sprite(img, 34, 54, 32, 53),
                            new Entities.helpers.Sprite(img, 0, 54, 32, 53)
							],
					//currentFrame : 0
					//currentFrame : 1
					currentFrame : 2
				},
				
                jumpRight:{
					frames: [new Entities.helpers.Sprite(img, 428, 0, 31, 53),new Entities.helpers.Sprite(img, 459, 0, 32, 53),
                            new Entities.helpers.Sprite(img, 493, 0, 32, 53)
							],
					//currentFrame : 0
					//currentFrame : 1
					currentFrame : 2
				} 
            };
			
            this.states = {
                jumping: {
                    movement: function (data) {
                        if (self.velY === 0) {
                            var jumpSound = self.jumpSound.cloneNode();
                            jumpSound.play();
                            self.velY -= 23;
                        }
                    },
                    animation: function (data) {
                        if (self.direction === "right") {
                            self.sprite = self.spriteAnimations.jumpRight.frames[self.spriteAnimations.jumpRight.currentFrame];
                        } else {
                            self.sprite = self.spriteAnimations.jumpLeft.frames[self.spriteAnimations.jumpLeft.currentFrame];
                        }
                    }
                },
                standing: {
                    movement: function (data) {
                        return;
                    },
                    animation: function (data) {
                        if (self.direction === "right") {
                            if (data.animationFrame % 30 === 0) {
                                self.sprite = self.spriteAnimations.standRight.frames[self.spriteAnimations.standRight.currentFrame];
                                self.spriteAnimations.standRight.currentFrame++;

                                if (self.spriteAnimations.standRight.currentFrame > 6) {
                                    self.spriteAnimations.standRight.currentFrame = 0;
                                }
                            }
                        }else {
                            if (data.animationFrame % 30 === 0) {
                                self.sprite = self.spriteAnimations.standLeft.frames[self.spriteAnimations.standLeft.currentFrame];
                                self.spriteAnimations.standLeft.currentFrame++;

                                if (self.spriteAnimations.standLeft.currentFrame > 6) {
                                    self.spriteAnimations.standLeft.currentFrame = 0;
                                }
                            }
                        }
                    }
                },
                walking: {
                    movement: function (data) {
                        if (self.direction === "right") {
                            self.x += self.velX;
                        } else {
                            self.x -= self.velX;
                        }
                    },
                    animation: function (data) {
                        if (self.direction === "right") {
                            if (data.animationFrame % 5 === 0) {
                                self.sprite = self.spriteAnimations.walkRight.frames[self.spriteAnimations.walkRight.currentFrame];
                                self.spriteAnimations.walkRight.currentFrame++;

                                if (self.spriteAnimations.walkRight.currentFrame > 6) {
                                    self.spriteAnimations.walkRight.currentFrame = 0;
                                }
                            }
                        } else {
                            if (data.animationFrame % 5 === 0) {
                                self.sprite = self.spriteAnimations.walkLeft.frames[self.spriteAnimations.walkLeft.currentFrame];
                                self.spriteAnimations.walkLeft.currentFrame++;

                                if (self.spriteAnimations.walkLeft.currentFrame > 6) {
                                    self.spriteAnimations.walkLeft.currentFrame = 0;
                                }
                            }
                        }
                    }
                }
            };
            this.currentState = self.states.standing;
            this.direction = "right";
            this.velY = 0;
            this.velX = 3.8;
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        },

        Coin: function (img, x, y, w, h) {
            var self = this;
            this.type = "coin";
            this.sound = new Audio("static/picked_Coin.wav");
            this.sprite = new Entities.helpers.Sprite(img, 100, 63, 18, 18);
            this.spriteAnimations = {
                spin: {
                    frames: [new Entities.helpers.Sprite(img, 262, 110, 8, 18), new Entities.helpers.Sprite(img, 272, 110, 14, 18),
                             new Entities.helpers.Sprite(img, 288, 110, 18, 18), new Entities.helpers.Sprite(img, 308, 110, 18, 18),
							 new Entities.helpers.Sprite(img, 328, 110, 18, 18), new Entities.helpers.Sprite(img, 348, 110, 16, 18),
                             new Entities.helpers.Sprite(img, 366, 110, 12, 18), new Entities.helpers.Sprite(img, 380, 110, 6, 18)
							],
                    currentFrame: 0
                }
            };
            this.states = {
                spinning: {
                    animation: function (data) {
                        if (data.animationFrame % 13 === 0) {
                            self.sprite = self.spriteAnimations.spin.frames[self.spriteAnimations.spin.currentFrame];
                            self.spriteAnimations.spin.currentFrame++;

                            if (self.spriteAnimations.spin.currentFrame > 7) {
                                self.spriteAnimations.spin.currentFrame = 0;
                            }
                        }
                    }
                }
            };
            this.currentState = self.states.spinning;
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        },

        Wall: function (x, y, w, h) {
            this.type = "wall";
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        },

        ExitPipe: function (x, y, w, h) {
            this.type = "exitPipe";
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        },
		
		Score: function(x,y,txt,visible){
			this.value = 0;
			this.x = x;
			this.y = y;
			this.txt = txt;
			this.visible = visible;
			this.size = "25px";
			this.font = "PixelEmulator";
			this.color = "white";
		},
		
		WinText: function(x,y,txt,visible){
			this.value = 0;
			this.x = x;
			this.y = y;
			this.txt = txt;
			this.visible = visible;
			this.size = "25px";
			this.font = "PixelEmulator";
			this.color = "white";
		}
    }
};
