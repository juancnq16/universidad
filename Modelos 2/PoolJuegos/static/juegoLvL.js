var Juego = {
    init: function () {
        var bgCanvas = document.getElementById("bg-canvas");
        var fgCanvas = document.getElementById("fg-canvas");

        var canvas = {
            bgCanvas: bgCanvas,
            fgCanvas: fgCanvas,
            bgCtx: bgCanvas.getContext("2d"),
            fgCtx: fgCanvas.getContext("2d")
        };

        var musicaFondo = new Audio("audio/background_Music.mp3");
        musicaFondo.loop = true;

        var spriteSheet = new Image();
        spriteSheet.src = "static/sprite_sheet(4).png";

        spriteSheet.addEventListener("load", function () {
            var spriteSheet = this;

            var datos = {
                animationFrame: 0,
                spriteSheet: spriteSheet,
                canvas: canvas
            };

            musicaFondo.play();

            Input.init(datos);
            Entities.init(datos);
            Render.init(datos);
            Juego.run(datos);
        });
    },

    run: function (datos) {
        var loop = function () {
            Juego.input(datos);
            Juego.update(datos);
            Juego.render(datos);
            datos.animationFrame++;
            window.requestAnimationFrame(loop);
        };
        loop();
    },
	
    input: function (datos) {
        Input.update(datos);
    },
    
	update: function (datos) {
        Animation.update(datos);
        Movement.update(datos);
        Physics.update(datos);
    },

    render: function (datos) {
        Render.update(datos);
    }
};

Juego.init();
