import sys, random, json
from flask import Flask, render_template, request, redirect, Response, make_response, jsonify, session
import db
app= Flask(__name__)
app.secret_key = "13579"
@app.route("/")
def output():
    return render_template("index.html")
@app.route("/SimonDice")
def simon():
    return render_template("simon.html")
@app.route("/flappyBird")
def flappy():
    return render_template("flappyBird.html")
@app.route("/quicaStein")
def quicaStein():
    return render_template("quicaStein.html")
@app.route("/subLvL1")
def subLvL1():
    return render_template("SubLvL1.html")
@app.route("/alienInvasion")
def alienInvasion():
    return render_template("alienInvasion.html")
@app.route("/laLlorona")
def laLlorona():
    return render_template("laLlorona.html")
@app.route("/snake")
def snake():
    return render_template("snake.html")
@app.route("/login", methods = ['POST'])
def logeo():
    session.clear()
    print("-------------")
    nombre=request.form["nombre"]
    if db.consultaJugador(nombre):
        session["usuario"]=nombre
    else:
        db.registrarJugador(nombre)
        session["usuario"]=nombre
    return render_template("inicio.html")
@app.route("/player", methods = ['POST'])
def player():
    cadena = {"player":session["usuario"]}
    json_str = json.dumps(cadena)
    return json_str
@app.route("/sugerencia", methods = ['POST'])
def sugerencia():
    sugerencia=db.sugerencia(session["usuario"])
    cadena = {"sugerencia":sugerencia}
    json_str = json.dumps(cadena)
    return json_str
@app.route("/top", methods = ['POST'])
def top():
    top = db.topPlayer()
    top = str(top)
    cadena = {"top":top}
    json_str= json.dumps(cadena)
    return json_str
@app.route("/capturar", methods = ['POST'])
def capturar():
    try:
        juego = request.form["juego"]
        puntaje = int(request.form["puntaje"])
        puntaje = puntaje
        jugador = session["usuario"]
        #print("datos capturados: ",jugador, puntaje, juego)
        db.insertarPuntaje(jugador, puntaje, juego)
        return "melo"
    except:
        return 'bad request!', 400
@app.route("/inicio", methods = ['POST','GET'])
def inicio():
    return render_template("inicio.html")
if __name__=="__main__":
    app.run(debug = True)