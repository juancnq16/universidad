import System.Random 

escribir::String->IO()
escribir m = (do putStr m
                 putStr "\n")

getInt :: IO Int
getInt = (do line <- getLine
             return (read line :: Int))

funcion :: Int->Int
funcion x = x+1

comparar :: Int->IO()
comparar x = 
    (do escribir "Ingrese un numero"
        n<-getInt
        if n == x then escribir "adivinaste"
        else
            if n<x then (do escribir "quedaste corto"
                            comparar x)
            else (do escribir "te sobra"
                     comparar x)
    )
juego::IO()
juego = 
    (do x<-randomRIO(1,100)
        comparar(funcion x)
    )
aleatorio x y = randomRIO(x,y)
main = 
    (do escribir "digite un numero"
        a<-getInt 
        escribir "digite otro numero"
        b<-getInt 
        c<-aleatorio a b
        escribir "termino"
        print c
    )