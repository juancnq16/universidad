import System.Random 

aleatorio a b = randomRIO(1,6)

escribir::String->IO()
escribir m = (do putStr m
                 putStr "\n")

getInt :: IO Int
getInt = (do line <- getLine
             return (read line :: Int))

jugar mazoJ = do
    escribir "tu mazo es :"
    putStrLn (show ( mazoJ ))
    escribir "deseas pedir una carta"
    entrada<-getInt
    if entrada==1 
        then (do mazo !!randomRIO(1,52) : mazoJ 
                 jugar mazoJ )
        else (do escribir "termina tu turno"
                 return mazoJ)

sumar mazoM suma =
    if mazoM then (do let suma = suma + head mazoM!!0
                      sumar tail mazoM suma)
    else return (suma)
jugarHost mazoH = do
    mazo !!randomRIO(1,52) : mazoH
    let suma1 = sumar mazoH 0
    if suma1 == 21 then (do jugarHost mazoH)
    else (do return mazoH)

veredicto mazo1 mazo2 = do
    let sumaH = sumar mazo1 0
    let sumaP = sumar mazo2 0
    if sumaH >21 
        then if sumaP <21 then escribir"gana el jugador"
    else if sumaP>21 then escribir "gana la casa"
    else if sumaH>sumaP then escribir "la casa gana"
    else "el jugador gana"
holaMundo = ':':'P':" Hola mundo"
pinta p = zip [0..13] (repeat p)
mazo = zip (take 52 (cycle[1..13])) (take 52 (cycle["picas","corazones","treboles","diamantes"]))
main = do
    let mazoHost=jugarHost []
    let mazoJugador=jugar []
    veredicto mazoHost mazoJugador
    
