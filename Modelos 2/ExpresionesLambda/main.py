def retornaMazo():
    pintas=["picas","corazones","diamantes","treboles"]
    valores=["Az",2,3,4,5,6,7,8,9,10,"J","Q","K"]
    mazo=[]
    for pinta in pintas:
        for valor in valores:
            if pinta=="picas" or pinta=="treboles":
                color="negro"
            else:
                color="rojo"
            carta=(color,pinta,valor)
            mazo.append(carta)
    return mazo
from functools import reduce
product = reduce((lambda x, y: x+1 * y+1) , [1, 2, 3, 4])
#print(product)
number_list = range(-5, 5)
less_than_zero = list(filter(lambda x: x < 0, number_list))
#print(less_than_zero)
factorial = reduce((lambda x,y: x*y),[1,2,3,4,5])
#print(factorial)
mazo=retornaMazo()
#print(mazo)
rojas=list(filter((lambda x: x[0]=="rojo"),mazo))
temp=list(filter((lambda x: type(x[2])==int),rojas))
lista=[]
for elem in temp:
    lista.append(elem[2])
a=[2,4,6,8,10]
sol=reduce((lambda x,y:x+y),lista)
print(sol)