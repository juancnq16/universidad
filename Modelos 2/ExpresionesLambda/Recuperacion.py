from functools import reduce
matriz=[[1,2,3],[4,5,6],[7,8,9]]
lista=[1,2,3,4,5,10,15,20,22]
lista2=[2,10,20,30,40]
lista3=[(1,2),(2,3),(3,3),(3,6)]
sol=list(map((lambda x:[x[0],x[len(x)-1]] ),matriz))
pre=list(map((lambda x: str(x)),lista))
sol2=list(filter((lambda x: filter((lambda y: int(y%2)==0),x)),pre))
sol3=list(map((lambda x: reduce((lambda x, y: x if x>y else y),x)),matriz))
sol4=list(
    filter(
        (lambda x: x>lista2[0]**5),lista2
    )
)
prueba = lambda x: range(x+1)
sol5=reduce(
        (lambda x,y:x[0]+y[0]),list(
            #aca van los x que sean triangulares de y
            filter(
                (lambda xz:xz[1]==reduce(
                    (lambda xy,yy:xy+yy),prueba(xz[0]))
                )
                ,lista3
            )
        )
    )
print(sol4, "los elementos que son mayores a la quinta potencia de la cabeza")
print(sol3, "los mayores elementos de la lista de listas")
print(sol,"los primeros y ultimos elementos de la lista de listas")
print(sol5, "la suma de los que son triangulares")