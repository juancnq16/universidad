function aleatorio(piso,techo){
	return Math.floor(Math.random() * (techo - piso + 1)) + piso;
}
function Spawn(){
    var cteEjemplo = 0
    this.cuadro=$("#cuadro")[0]
    this.hablando=$("#DoomGuy")[0]
    this.apuntando=$("#Aiming")[0]
    this.stop=$("#stop")[0];
    this.ordenes=[$("#arriba")[0], $("#izquierda")[0], $("#abajo")[0], $("#derecha")[0]];
    this.index=0
    this.musica=document.getElementsByTagName("audio")[0];
    this.puntaje=0;
    /*this.ordenes=[37,38,39,40];
    0=arriba=38=87
    1=izquierda=37=65
    2=abajo=40=83
    3=derecha=39=68
    */
    this.orden=this.ordenes[this.index];
    this.actualizar = function(digito){
        cteEjemplo+=digito;
        console.log(cteEjemplo);
        
    }
    this.dialogar = function(texto, contexto){
        contexto.drawImage(this.cuadro,0,0);
        contexto.drawImage(this.hablando,250,250)       
        contexto.fillStyle = "#000000";
		contexto.font = "25px sans-serif";
        contexto.fillText(texto, 70, 130);
        this.dibujarPuntaje(contexto);
        contexto.save();
        contexto.restore();
    }
    this.ordenar = function(contexto){
        this.musica.play();
        this.index = aleatorio(0,3);
        this.orden=this.ordenes[this.index];
        contexto.drawImage(this.cuadro,0,0);
        contexto.drawImage(this.apuntando, 250, 250);
        contexto.drawImage(this.orden,120, 50);
        this.dibujarPuntaje(contexto);
        contexto.save();
        contexto.restore();
    }
    this.juzgar = function(comando){
        console.log(comando);
        if(comando==38||comando==87 && this.index==0){
            return true;
        }else if(comando==37||comando==65 && this.index==1){
            return true;
        }else if(comando==40||comando==83 && this.index==2){
            return true;
        }else if(comando==39||comando==68 && this.index==3){
            return true;
        }else{
            return false;
        }
    }
    this.detener = function(contexto){
        this.musica.pause();
        contexto.drawImage(this.cuadro,0,0);
        contexto.fillStyle = "#000000";
		contexto.font = "25px sans-serif";
        contexto.fillText("PRESIONA ENTER!", 70, 130);
        contexto.drawImage(this.stop, 250, 200);
        this.dibujarPuntaje(contexto);
        contexto.save();
        contexto.restore();
    }
    this.dibujarPuntaje = function(contexto){
        //console.log(this.puntaje);
        contexto.fillStyle = "#000000";
		contexto.font = "25px sans-serif";
        contexto.fillText("tu puntaje es: "+this.puntaje, 70, 500);
        contexto.save();
        contexto.restore();
    }
    
}
