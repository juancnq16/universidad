/*dijkstra 1 es una funcion de inicializado
y llenado de las colecciones necesarias*/
function IniciarColecciones(n,mat){
    let marcados=[];
    let distancias=[];
    let padres = [];
    let ad = [];
    var matPrueba = [
        [0,1,2,0,0,0,0],
        [1,0,3,0,0,0,0],
        [2,3,0,2,9,7,2],
        [0,0,2,0,1,0,0],
        [0,0,9,1,0,1,0],
        [0,0,7,0,1,0,1],
        [0,0,2,0,0,1,0]
    ]
    for(let i=0 ; i<n ; i++){
        ad.push([]);
        marcados.push(0);
        distancias.push(0);
        padres.push(0)
        for(let j=0 ; j<n ; j++){
            ad[i].push(0);
        }
    }
    dijsktraInicial(marcados,distancias,padres,mat,1,n);
}
/*
dijkstra2 es la funcion recursiva que dado un grafo con el 
nodo inicial extendido determinara el camino mas corto
hacia cada uno de los nodos del grafo
*/
function dijkstraRecursivo(marcados,distancias,padres,ad,size){
    console.log(marcados);
    console.log(distancias);
    let menor;
    //este bloque busca un nodo extensible
    for(let i=0;i<size;i++){
        if (distancias[i]!=0 && marcados[i]==0){
            menor=i;
        }
    }
    //este bloque determina si el posible es el menor
    for(let i=0;i<size;i++){
        if(distancias[i]!=0 && distancias[menor]!=0){   
            if(distancias[i]<distancias[menor] ){
                if(marcados[i]==0){
                    menor=i;
                }
            }
        }
    }
    marcados[menor]=1;
    console.log("se ha marcado "+menor);
    /*
    estos bloques contienen el bloque principal del algoritmo
    el cual extiende el grafo determinado para determinar la ruta mas cortadista
    */
    for(let j=0 ; j<size ; j++){
        if(ad[menor][j]!=0){
            if (marcados[j]==0){
                if(distancias[j] > distancias[menor] + ad[menor][j] && distancias[j]!=0){
                    distancias[j]=distancias[menor] + ad[menor][j];
                    padres[j]=menor;
                    //console.log("se ha cambiado la distancia de "+j+" A "+distancias[menor] + ad[menor][j])
                }else if (distancias[j]==0){
                    distancias[j]=distancias[menor] + ad[menor][j];
                    padres[j]=menor;
                    //console.log("se ha cambiado la distancia de "+j+" A "+distancias[menor] + ad[menor][j])
                }
            }
        }
    }
    let bool = true;
    for(let k=0;k<size;k++){
        if(marcados[k]==0){
            bool=false;
        }
    }
    if (bool){
        console.log("resultados:")
        console.log(marcados);
        console.log(distancias);
        console.log(padres);
        console.log(ad);
        var resultados=document.getElementById("resultados");
        var etiqueta = document.createElement("div");
        var etiqueta2 = document.createElement("div");
        var etiqueta3 = document.createElement("div");
        var salto = document.createElement("br")
        etiqueta.id="distancias";
        etiqueta2.id="marcados";
        etiqueta3.id="padres";
        etiqueta.innerHTML=distancias;
        etiqueta2.innerHTML=marcados;
        etiqueta3.innerHTML=padres;
        resultados.appendChild(etiqueta);
        resultados.appendChild(salto);
        resultados.appendChild(etiqueta2);
        resultados.appendChild(salto);
        resultados.appendChild(etiqueta3);
        resultados.appendChild(salto);
        return distancias;
    }else{
        return dijkstraRecursivo(marcados,distancias,padres,ad,size)
    }
}
/*
dijkstraInicial es encargada de extender el primer nodo y desencadenar
la recursion
*/
function dijsktraInicial(marcados,distancias,padres,ad,nodo,size) {
    for(let i=0;i<size;i++){
        if(ad[nodo-1][i]!=0){
            distancias[i]=ad[nodo-1][i];
            padres[i]=nodo-1;
        }
    }
    marcados[nodo-1]=1;
    dijkstraRecursivo(marcados,distancias,padres,ad,size);
}
/*
esta funcion dibuja la matriz en el html dado un n
para su dimension
*/
function dibujaMatriz(){
    var matriz = document.getElementById("matriz");
    var n = parseInt(document.getElementById("n").value);
    for (let i=0; i<n; i++){
        for(let j=0 ; j<n ; j++){
            var casilla = document.createElement("input");
            casilla.type="text";
            casilla.id = i.toString()+j.toString();
            casilla.value="0";
            casilla.size="4";
            matriz.appendChild(casilla);
        }
        var division = document.createElement("br");
        matriz.appendChild(division);
    }
}
/*
matWrap esta encargada de envolver la matriz del DOM
para poder usarla dentro del algoritmo
*/
function matWrap(){
    var arr = [];
    var n = parseInt(document.getElementById("n").value);
    console.log("de aca para abajo es el wrap")
    for(let i=0; i<n ; i++){
        arr.push([]);
        for(let j=0; j<n; j++){
            arr[i].push(parseInt(document.getElementById(i.toString()+j.toString()).value));
            console.log(document.getElementById(i.toString()+j.toString()).value);
        }
    }
    console.log(arr);
    IniciarColecciones(n,arr);
}
//un comentario aleatorio